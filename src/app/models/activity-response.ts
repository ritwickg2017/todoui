import { IActivity } from './activity';
export interface IActivityResponse {
    statusCode: number;
    activityList: IActivity[];
    activityData: IActivity;
    message: string;
}
