export interface IActivityDisplay {
    displayName: string;
    property: string;
}
