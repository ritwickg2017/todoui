import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IActivity } from '../models/activity';
import { IActivityResponse } from '../models/activity-response';

@Injectable({
    providedIn: 'root',
})
export class ToDoService {
    private baseUrl: string;
    constructor(private http: HttpClient) {
        this.baseUrl = environment.apiDomain;
    }

    addActivity(activity: IActivity): Observable<IActivityResponse> {
        return this.http.post<IActivityResponse>(this.baseUrl + '/addTodoActivity', activity);
    }
    getActivityByEmailId(email: string): Observable<IActivityResponse> {
        return this.http.post<IActivityResponse>(this.baseUrl + '/getTodoList/', { createdBy: email });
    }
}
