import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UserService } from 'src/app/core/services/user.service';
import { IActivity } from '../../models/activity';
import { ToDoService } from '../to-do.service';

@Component({
    selector: 'app-add-activity',
    templateUrl: './add-activity.component.html',
    styleUrls: ['./add-activity.component.css'],
})
export class AddActivityComponent implements OnInit {
    addActivityFormGroup: FormGroup;
    public readonly activityList: any = [
        {
            displayName: 'Pending',
            value: 'pending',
        },
    ];
    constructor(
        private formBuilder: FormBuilder,
        private toDoService: ToDoService,
        private userService: UserService,
        private snackbar: MatSnackBar,
        private router: Router,
    ) {}

    ngOnInit(): void {
        this.addActivityFormGroup = this.generateAddFormGroup();
    }
    generateAddFormGroup(): FormGroup {
        return this.formBuilder.group({
            createdDate: ['', [Validators.required]],
            activityDate: ['', [Validators.required]],
            status: ['', [Validators.required]],
            name: ['', [Validators.required]],
        });
    }
    addActivity() {
        const activity: IActivity = this.addActivityFormGroup.value;
        activity.createdBy = this.userService.signedInUser.getUsername();
        this.toDoService.addActivity(activity).subscribe((response) => {
            this.snackbar
                .open(response.message, 'Close', {
                    duration: 2000,
                })
                .afterDismissed()
                .subscribe((afterDismissed) => {
                    this.router.navigate(['/todo']);
                });
        });
    }
}
