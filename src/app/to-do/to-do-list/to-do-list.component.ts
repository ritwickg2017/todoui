import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../core/services/user.service';
import { ToDoService } from '../to-do.service';
import { IActivity } from '../../models/activity';
import { IActivityDisplay } from '../../models/activity-display';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
    selector: 'app-to-do-list',
    templateUrl: './to-do-list.component.html',
    styleUrls: ['./to-do-list.component.css'],
})
export class ToDoListComponent implements OnInit, AfterViewInit {
    public readonly displayedColumns: IActivityDisplay[] = [
        {
            displayName: 'Name',
            property: 'name',
        },
        {
            displayName: 'Created By',
            property: 'createdBy',
        },
        {
            displayName: 'Created Date',
            property: 'createdDate',
        },
        {
            displayName: 'Status',
            property: 'status',
        },
    ];
    public readonly columnsToDisplay = this.displayedColumns.map((x) => x.displayName).slice();
    public activityData: MatTableDataSource<IActivity> = new MatTableDataSource();
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

    constructor(private router: Router, private toDoService: ToDoService, private userService: UserService) {}

    ngOnInit(): void {
        this.toDoService.getActivityByEmailId(this.userService.signedInUser.getUsername()).subscribe((response) => {
            console.log(response.activityList);
            this.activityData.data = response.activityList;
        });
    }
    ngAfterViewInit(): void {
        this.activityData.paginator = this.paginator;
    }

    redirectToAddActivity() {
        this.router.navigate(['/todo/add-activity']);
    }
}
